begin
  require 'spec'
rescue LoadError
  require 'rubygems'
  require 'spec'
end
begin
  require 'spec/rake/spectask'
rescue LoadError
  puts <<-EOS
To use rspec for testing you must install rspec gem:
    gem install rspec
EOS
  exit(0)
end

desc "Run the specs under spec/models"
Spec::Rake::SpecTask.new do |t|
  t.spec_opts = ['--options', "spec/spec.opts"]
  t.spec_files = FileList['spec/**/*_spec.rb']
end

namespace :spec do
  desc "Start Autotest"
  task :autotest do
    require File.join(File.dirname(__FILE__), '..', 'spec', 'rspec_autotest')
    RspecAutotest.run
  end
  
  desc "run specs with rcov"
  Spec::Rake::SpecTask.new(:rcov) do |t|
    t.spec_opts = ['--options', "spec/spec.opts"]
    t.spec_files = FileList['spec/**/*_spec.rb']
    t.rcov = true
    t.rcov_dir = './doc/coverage'
    t.rcov_opts = ['--exclude', 'spec\/spec_helper.rb,spec\/rspec_autotest.rb']
  end
end

# desc "Default task is to run specs"
# task :default => :spec
Rake.application[:default].prerequisites.gsub! /test/, 'spec'